<?php
/**
	* Test 2203 Realty
	*
	* Plugin Name:  Test 2203 Realty
	* Description:  Тестовое задание База недвижимости
	* Version:      0.3
	* Author:       Sergey Davydov
	* Author URI:   https://gitlab.com/MrSwed
	* Text Domain:  realty
	*
	*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'realty_fields' ) ) {
	/**
		* Get additional meta fields for realty posts
		*
		* @return array[]
		*/

	function realty_fields(): array {
		return [
			'realty_squire'      => [
				'label' => __( "Площадь, м<sup>2</sup>", "realty" ),
				'type'  => "number",
			],
			'realty_rent'        => [
				'label' => __( "Стоимость", "realty" ),
				'type'  => "number",
			],
			'realty_address'     => [
				'label' => __( "Адрес", "realty" ),
				'type'  => 'textarea',
			],
			'realty_live_squire' => [
				'label' => __( "Жилая площадь, м<sup>2</sup>", "realty" ),
				'attr'  => [
					'type' => "number",
				],
			],
			'realty_floor'       => [
				'type'  => "number",
				'label' => __( "Этаж", "realty" ),
			],
			'realty_city'        => [
				'type'    => "select",
				'options' => realty_get_cities(),
				'label'   => __( "Город", "realty" ),
			],
			'realty_photos'      => [
				'type'                       => "images",
				'label'                      => __( "Изображения", "realty" ),
				'sanitize_callback'          => 'realty_attachment_urls_to_post_id',
				'realty_parameters_excluded' => true
			],
		];
	}
}
if ( ! function_exists( 'realty_types' ) ) {


	/**
		* Register Realty Type
		*
		* @return void
		*/
	function realty_types() {
		register_taxonomy( 'realty_type', array( 'realty' ), array(
			'labels'            => array(
				'name'          => _x( 'Тип', 'Taxonomy General Name', 'realty' ),
				'singular_name' => _x( 'Тип недвижимости', 'Taxonomy Singular Name', 'realty' ),
				'menu_name'     => __( 'Тип недвижимости', 'realty' ),
			),
			'hierarchical'      => false,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
		) );

		register_post_type( 'realty', array(
			'label'               => __( 'Недвижимость', 'realty' ),
			'description'         => __( 'Недвижимость', 'realty' ),
			'labels'              => array(
				'name'                  => _x( 'Недвижимость', 'Post Type General Name', 'realty' ),
				'singular_name'         => _x( 'Недвижимость', 'Post Type Singular Name', 'realty' ),
				'menu_name'             => __( 'Недвижимость', 'realty' ),
				'name_admin_bar'        => __( 'Недвижимость', 'realty' ),
				'all_items'             => __( 'Вся Недвижимость', 'realty' ),
				'add_new_item'          => __( 'Добавить Недвижимость', 'realty' ),
				'add_new'               => __( 'Добавить Недвижимость', 'realty' ),
				'new_item'              => __( 'Новая Недвижимость', 'realty' ),
				'edit_item'             => __( 'Редактировать', 'realty' ),
				'update_item'           => __( 'Обновить', 'realty' ),
				'view_item'             => __( 'Просмотр', 'realty' ),
				'view_items'            => __( 'Смотреть все', 'realty' ),
				'search_items'          => __( 'Поиск', 'realty' ),
				'not_found'             => __( 'Не найдено', 'realty' ),
				'items_list'            => __( 'Список Недвижимости', 'realty' ),
				'items_list_navigation' => __( 'Навигация по Недвижимости', 'realty' ),
				'filter_items_list'     => __( 'Фильтровать', 'realty' ),
			),
			'supports'            => array( 'title', 'excerpt', 'editor', 'thumbnail', 'page-attributes' ),
			'taxonomies'          => array( 'realty_type' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_rest'        => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-bank',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => array(
				'slug'       => 'realty',
				'with_front' => true,
				'pages'      => true,
				'feeds'      => true,
			),
			'capability_type'     => 'post',
		) );

		register_post_type( 'realty_city', array(
			'label'               => __( 'Город', 'realty' ),
			'description'         => __( 'Город', 'realty' ),
			'labels'              => array(
				'name'                  => _x( 'Города', 'Post Type General Name', 'realty' ),
				'singular_name'         => _x( 'Город', 'Post Type Singular Name', 'realty' ),
				'menu_name'             => __( 'Города', 'realty' ),
				'name_admin_bar'        => __( 'Город', 'realty' ),
				'all_items'             => __( 'Все Города', 'realty' ),
				'add_new_item'          => __( 'Добавить Город', 'realty' ),
				'add_new'               => __( 'Добавить Город', 'realty' ),
				'new_item'              => __( 'Новый Город', 'realty' ),
				'edit_item'             => __( 'Редактировать', 'realty' ),
				'update_item'           => __( 'Обновить', 'realty' ),
				'view_item'             => __( 'Просмотр', 'realty' ),
				'view_items'            => __( 'Смотреть все', 'realty' ),
				'search_items'          => __( 'Поиск', 'realty' ),
				'not_found'             => __( 'Не найдено', 'realty' ),
				'not_found_in_trash'    => __( 'Корзина пуста', 'realty' ),
				'featured_image'        => __( 'Изображение', 'realty' ),
				'set_featured_image'    => __( 'Установить изображение', 'realty' ),
				'remove_featured_image' => __( 'Убрать изображение', 'realty' ),
				'use_featured_image'    => __( 'Использовать изображение', 'realty' ),
				'insert_into_item'      => __( 'Вставить в запись', 'realty' ),
				'uploaded_to_this_item' => __( 'Загружено в запись', 'realty' ),
				'items_list'            => __( 'Список Недвижимости', 'realty' ),
				'items_list_navigation' => __( 'Навигация по Недвижимости', 'realty' ),
				'filter_items_list'     => __( 'Фильтровать', 'realty' ),
			),
			'supports'            => array( 'title', 'excerpt', 'editor', 'thumbnail', 'page-attributes' ),
			'taxonomies'          => array(),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_rest'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-location',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		) );

		foreach ( realty_fields() as $k => $field ) {
			$args = [ 'single' => true, 'show_in_rest' => true ];
			if ( isset( $field['sanitize_callback'] ) ) {
				$args['sanitize_callback'] = $field['sanitize_callback'];
			}
			register_post_meta( "realty", "_" . $k, $args );
		}
	}

	add_action( 'init', 'realty_types', 0 );

}
if ( ! function_exists( 'realty_metaboxes' ) ) {

	/**
		* Add meta Box for admin
		*
		* @return void
		*/
	function realty_metaboxes() {
		$screens = [ 'realty' ];
		foreach ( $screens as $screen ) {
			add_meta_box( 'realty_metabox', 'Параметры недвижимости', 'realty_metabox_html', $screen, 'side', 'high' );
		}
	}

	add_action( 'add_meta_boxes', 'realty_metaboxes' );
}
if ( ! function_exists( 'realty_metabox_html' ) ) {
	/**
		* Generate Metabox html
		*
		* @param $post
		*
		* @return void
		*/
	function realty_metabox_html( $post = null ) {
		$fields = realty_fields();
		foreach ( $fields as $key => $field ) {
			$field['type'] = ! empty( $field['type'] ) ? $field['type'] : 'text';
			$value         = ( $post and $post->ID ) ? get_post_meta( $post->ID, '_' . $key, true ) : '';
			if ( 'number' === $field['type'] and ! isset( $field['attr']['min'] ) ) {
				$field['attr']['min'] = 0;
			}
			if ( 'textarea' === $field['type'] ) {
				$field['attr']['rows'] = ! empty( $field['attr']['rows'] ) ? $field['attr']['rows'] : 2;
//						$field['attr']['cols'] = !empty($field['attr']['cols']) ? $field['attr']['cols'] : 20;
			}
			$attr = [];
			if ( ! empty( $field['attr'] ) and is_array( $field['attr'] ) ) {
				foreach ( $field['attr'] as $ak => $av ) {
					$attr[] = "$ak=\"$av\"";
				}
			}
			?>
			<p class="postbox-header">
				<label for="<?php echo $key; ?>"><?php echo $field['label'] ?></label>
				<?php
				switch ( $field['type'] ) {
					case "textarea":
						?><textarea name="<?php echo $key; ?>" id="<?php echo $key; ?>" <?php
						echo implode( " ", $attr );
						?>><?php echo esc_textarea( $value ) ?></textarea><?php
						break;
					case "select":
						?><select name="<?php echo $key; ?>" id="<?php echo $key; ?>" <?php
						echo implode( " ", $attr );
						?>><?php
						if ( ! empty( $field["options"] ) ) {
							if ( ! isset( $field["options"][''] ) ) {
								echo "<option value=''></option>\n";
							}
							foreach ( (array) $field["options"] as $val => $label ) {
								echo "<option value='$val' " . ( $val == $value ? ' selected="selected"' : '' ) . ">$label</option>\n";
							}
						} ?>
						</select><?php
						break;
					case "images":
						if ( ! $value ) {
							$value = get_post_thumbnail_id( $post->ID );
						}
						?><span><input name="<?php echo $key; ?>" type="hidden" id="<?php echo $key; ?>"
						               value="<?php echo esc_attr( $value ) ?>"/>
						<a href="#" data-gallery="[gallery ids='<?php echo $value; ?>']" data-target="<?php echo $key; ?>"
						   onclick="let t=this;wp.media.gallery.edit(jQuery(t).data('gallery')).on('update',function(obj) {
							   let $=jQuery, 
										_i=$('#'+$(t).data('target')), 
							   _ids=obj.models.map((i)=>i.id).join(',');
							   if(_i.length)_i.val(_ids);
										$(t).data('gallery','[gallery ids=\''+_ids+'\']').parent().find('.count').text(_ids.split(',').length);
							   });return false;">Выбрать изображения</a><br>
						<?php echo __( "Выбрано", 'realty' ) . " <span class='count'>" . count( wp_parse_id_list( $value ) ) . "</span>"; ?>
						</span>
						<?php
						break;
					case "number":
					case "text":
					default:
						?><input name="<?php echo $key; ?>" type="<?php echo $field['type']; ?>" id="<?php echo $key; ?>"
						         value="<?php echo esc_attr( $value ) ?>" <?php
						echo implode( " ", $attr );
						?>/><?php
				}
				?>
			</p>
			<?php
		}
	}
}
if ( ! function_exists( 'realty_save_meta' ) ) {
	/**
		* Save realty additional meta
		*
		* @param $post_id
		*
		* @return void
		*/
	function realty_save_meta( $post_id ) {
		if ( "realty" === get_post_type( $post_id ) and ! empty( $_POST ) ) {
			$attr = filter_input_array( INPUT_POST, array(
				'realty_squire'      => FILTER_SANITIZE_NUMBER_FLOAT,
				'realty_rent'        => FILTER_SANITIZE_NUMBER_FLOAT,
				'realty_address'     => FILTER_SANITIZE_STRING,
				'realty_live_squire' => FILTER_SANITIZE_NUMBER_FLOAT,
				'realty_floor'       => FILTER_SANITIZE_NUMBER_INT,
				'realty_city'        => FILTER_SANITIZE_NUMBER_INT,
				'realty_photos'      => FILTER_SANITIZE_STRING
			) );
			if ( $attr ) {
				foreach ( $attr as $field => $value ) {
					if ( ! in_array( $value, [ null, false ] ) ) {
						update_post_meta( $post_id, '_' . $field, $value );
					}
				}
			}
		}
	}

	add_action( 'save_post', 'realty_save_meta' );
}
if ( ! function_exists( 'realty_get_cities' ) ) {
	/**
		* Return array of realty cities
		*
		* @param array $args
		*
		* @return array|false|mixed
		*/
	function realty_get_cities( array $args = [] ) {
		$args = wp_parse_args( $args, [
			'post_type'   => 'realty_city',
			'post_status' => 'publish'
		] );
		$data = wp_cache_get( "realty_get_cities" );
		if ( ! $data ) {
			$data = wp_list_pluck( get_posts( $args ), "post_title", "ID" );
			wp_cache_add( "realty_get_cities", $data );
		}

		return $data;
	}
}
if ( ! function_exists( 'realty_parameters' ) ) {
	/**
		*
		* Print html with realty parameters
		*
		* @param $post_id
		* @param $exclude
		* @param string $attrs
		*
		* @return void
		*/
	function realty_parameters( $post_id = null, $exclude = null, string $attrs = '' ) {
		if ( $post_id == null ) {
			$post_id = get_the_ID();
		}
		$fields = realty_fields();
		$li     = [];
		if ( isset( $exclude ) and is_string( $exclude ) ) {
			$exclude = wp_parse_list( $exclude );
		}
		foreach ( $fields as $k => $f ) {
			if ( empty( $f['realty_parameters_excluded'] ) and ( empty( $exclude ) or ! in_array( $k, $exclude ) ) ) {
				$value = get_post_meta( $post_id, "_" . $k, true );
				if ( isset( $f['type'] ) and 'select' === $f['type'] and ! empty( $f["options"][ $value ] ) ) {
					if ( 'realty_city' === $k ) {
						$value = "<a href='" . esc_url( get_permalink( $value ) ) . "'>" . $f["options"][ $value ] . "</a>";
					} else {
						$value = $f["options"][ $value ];
					}
				}
				$li[] = sprintf( '<li><span class="name">%s</span>: <span class="value">%s</span>', $f["label"], $value );
			}
		}
		echo "<ul $attrs>" . implode( "\n", $li );
		echo "<li>";
		the_taxonomies( [ "post" => $post_id ] );
		echo "</li>";
		echo "</ul>";
	}
}
if ( ! function_exists( 'realty_forminator_field_select' ) ) {
	/**
		* Fill select option in forminator for special key=value
		*
		* @param $html
		*
		* @return array|mixed|string|string[]
		*/
	function realty_forminator_field_select( $html ) {
		$str_find = '<option value="realty_get_cities" data-calculation="0">realty_get_cities</option>';
		$str_new  = [];
		if ( false !== strpos( $html, $str_find ) ) {
			foreach ( realty_get_cities() as $val => $label ) {
				$str_new[] = "<option value='$val' data-calculation='0'>$label</option>";
			}
			$html = str_replace( $str_find, implode( "\n", $str_new ), $html );
		}

		return $html;
	}

	add_filter( "forminator_field_single_markup", "realty_forminator_field_select" );
}
if ( ! function_exists( 'realty_forminator_post_data_field_post_saved' ) ) {
	/**
		* Update new meta post from frominator plugin
		*
		* @param $post_id
		* @param $field
		* @param $data
		* @param $postdata
		*
		* @return void
		*/
	function realty_forminator_post_data_field_post_saved( $post_id, $field, $data, $postdata ) {
		$fFields = array_combine( array_column( $data["post-custom"], "key" ), array_column( $data["post-custom"], "value" ) );
		delete_post_meta( $post_id, Forminator_Base_Form_Model::META_KEY );
		delete_post_meta( $post_id, "_has_forminator_meta" );
		if ( isset( $fFields["_realty_photos"] ) ) {
			$fFields["_realty_photos"] = realty_attachment_urls_to_post_id( $fFields["_realty_photos"] );
			update_post_meta( $post_id, "_realty_photos", $fFields["_realty_photos"], true );
			$first = wp_parse_id_list( $fFields["_realty_photos"] );
			$first = reset( $first );
			set_post_thumbnail( $post_id, $first );
			wp_update_attachment_metadata( $post_id, wp_get_attachment_metadata( $first ) );
		}
	}

	add_action( 'forminator_post_data_field_post_saved', "realty_forminator_post_data_field_post_saved", 10, 4 );
}
if ( ! function_exists( 'realty_attachment_urls_to_post_id' ) ) {
	/**
		* Replace images urls at formitator form to it attachments ids separated by comma
		*
		* @param $meta_value
		*
		* @return string
		*/
	function realty_attachment_urls_to_post_id( $meta_value ): string {
		$meta_value = wp_parse_list( $meta_value );
		$meta_value = array_map( function ( $i ) {
			if ( ! is_numeric( $i ) ) {
				$in = attachment_url_to_postid( $i );
				if ( $in ) {
					$i = $in;
				}
			}

			return $i;
		}, wp_parse_list( $meta_value ) );

		return implode( ",", $meta_value );
	}
}
if ( is_admin() ) {
	if ( ! function_exists( 'realty_admin_manage_columns' ) ) {
		function realty_admin_manage_columns( $columns ) {
			$columns['realty']        = __( "Характеристики", "realty" );
			$columns['realty_photos'] = __( "Изображение", "realty" );

			return $columns;
		}

		add_filter( "manage_realty_posts_columns", "realty_admin_manage_columns" );
	}
	if ( ! function_exists( 'realty_admin_manage_column' ) ) {
		function realty_admin_manage_column( $column, $post_id ) {
			switch ( $column ) {
				case 'realty_photos':
					the_post_thumbnail( [ 60, 60 ] );
					break;
				case "realty":
					echo "<small>";
					realty_parameters( null, 'realty_photos', 'style="margin:0;line-height:0.5"' );
					echo "</small>";
					break;
			}
		}

		add_action( "manage_realty_posts_custom_column", "realty_admin_manage_column", 10, 2 );
	}
}